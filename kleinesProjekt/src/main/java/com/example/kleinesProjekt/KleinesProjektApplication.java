package com.example.kleinesProjekt;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KleinesProjektApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(KleinesProjektApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		hello hey = new hello();

		hey.hello();
		hey.hello();
		hey.hello();
	}
}

